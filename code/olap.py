import newt.db 
import pandas as pd


class Orders(newt.db.Persistent):
   def __init__(self):
       self.orders = newt.db.List()

   def add(self,order):
       print("order added")
       self.orders.append(order)
        

class Order(newt.db.Persistent):
    def __init__(self, orderDate, product, location, quantityOrdered, priceEach, sales):
        #Class Date // Dimension Time
        self.Date = orderDate
        #Class Product / Dimensions Product
        self.product = product
        #Class Location // Dimension Location
        self.location = location
        #Facts
        self.quantityOrdered = quantityOrdered
        self.priceEach = priceEach
        self.sales = sales

class Product(newt.db.Persistent):
    def __init__(self,productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP):
        self.productCode = productCode
        self.productName = productName
        #Class Productline
        self.productLine = productLine
        self.productScale = productScale
        self.productVendor = productVendor
        self.productDescription = productDescription
        self.quantityInStock = quantityInStock
        self.buyPrice = buyPrice
        self.MSRP = MSRP

class Productline(newt.db.Persistent):
    def __init__(self,textDescription,htmlDescription,image):
        self.textDescription = textDescription
        self.htmlDescription = htmlDescription
        self.image = image

class Location(newt.db.Persistent):
    def __init__(self, city, state, postalCode, country):
        self.city = city 
        self.state = state
        self.postalCode = postalCode
        self.country = country

class Cube(newt.db.Persistent):
    def __init__(self):
        self.orders = newt.db.List()

        self.sliceDate = newt.db.List()
        self.sliceProduct = newt.db.List()
        self.sliceLocation = newt.db.List()

        self.df = pd.DataFrame()
    
    def addOrders(self,data):
        print("...")
        self.orders = data
        self.generateDataframe()
        print("Orders added")
        print("-----------------")
    
    def addSliceLocation(self,ort):
        self.sliceLocation.append(ort)
        print("Location added: "+ort)
        print("-----------------")

    def addSliceDate(self,zeit):
        self.sliceDate.append(zeit)
        print("Date added: "+zeit)
        print("-----------------")
    
    def addSliceProduct(self,produkt):
        self.sliceProduct.append(produkt)
        print("Product added: "+produkt)
        print("-----------------")
    
    def generateDataframe(self):
        data = self.df
        for order in self.orders:
            entry = {
                "Date": str(order.Date.year),
                "Product": order.product.productName,
                "Location": order.location.country,
                "Sales": order.sales, 
                "Quantity": order.quantityOrdered
            }
            data = data.append(pd.DataFrame.from_records([entry]),ignore_index=True)
            self.df = data
    
    def computeSlices(self):

        print("Compute slices...")
        print("-----------------")

        
        df = self.df

        #Filter Location
        searchfor = self.sliceLocation
        df = df[df.Location.str.contains('|'.join(searchfor))]

        #Filter Product 
        searchfor = self.sliceProduct
        df = df[df.Product.str.contains('|'.join(searchfor))]

        #Filter Year
        searchfor = self.sliceDate
        df = df[df.Date.str.contains('|'.join(searchfor))]

        self.df = df
    
    def aggregate(self, aggregate, aggregateFunction):

        print("Aggregate ...")
        print("-----------------")
        
        #Aggregate Location
        if(aggregate=="Location"):
            df = self.df
            df = df.groupby(["Location"])['Sales'].agg(aggregateFunction)
            df = df.sort_values(by=aggregateFunction,ascending=False)
        
        #Aggregate Product
        elif(aggregate=="Product"):
            df = self.df
            df = df.groupby(["Product"])['Sales'].agg(aggregateFunction)
            df = df.sort_values(by=aggregateFunction,ascending=False)
        
        #Aggregate Date
        elif(aggregate=="Date"):
            df = self.df
            df = df.groupby(["Date"])['Sales'].agg(aggregateFunction)
            df = df.sort_values(by=aggregateFunction,ascending=False)    

        self.df = df
    
    def report(self):
        print("Report:")
        pd.set_option('display.max_rows', None)
        print (self.df)

    def clear(self):
        print("...")
        self.df = pd.DataFrame()
        self.generateDataframe()
        print("Cube cleared")