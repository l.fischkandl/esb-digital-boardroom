import newt.db
import olap

connection = newt.db.connection("postgresql://postgres:changeme@postgres:5432/newt")
orders = connection.root.orders.orders

cube = olap.Cube()
cube.addOrders(orders)
 

#In welchem Land wurde im Jahr 2003 der meiste Umsatz erzielt? 
cube.addSliceDate("2003")
cube.aggregate("Location",["sum"])
cube.report()
cube.clear()

#Welches Produkt hat in Australien den meisten Umsatz in 2003 erzielt? 
cube.addSliceDate("2003")
cube.addSliceLocation("Australia")
cube.aggregate("Product",["sum"])
cube.report()
cube.clear()

#In welchem Jahr wurde der meiste Umsatz erzielt?
cube.aggregate("Date",["sum"])
cube.report()
cube.clear()

