import newt.db
import mysql.connector
from olap import Orders
from olap import Order
from olap import Product
from olap import Productline
from olap import Location

def createObjectsAndWriteInDB():

    # MySQL Connection

    mydb = mysql.connector.connect(
    host="db",
    user="root",
    password="test",
    database="classicmodels"
    )

    def getResult(statement,var):
        cur = mydb.cursor()
        cur.execute(statement,var)
        r = [dict((cur.description[i][0], value)
                    for i, value in enumerate(row)) for row in cur.fetchall()]
        return r


    #Create Order Object 
    orders = Orders()

    #Get all Orders (One Order per product sold)
    statement = "SELECT * FROM orders RIGHT JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber"
    orders_db = getResult(statement,())

    #print(orders_db)

    for order_db in orders_db:

        #JOIN with products 
        productCode = order_db["productCode"]
        statement = "SELECT * FROM products LEFT JOIN productlines ON products.productLine = productlines.productLine WHERE productCode=%s"
        r = getResult(statement,(productCode,))
        product_db = r[0]

        #Create Product Line Object

        productline = Productline(
            product_db["textDescription"],
            product_db["htmlDescription"],
            product_db["image"]
        )

        #Create Product Object

        product = Product(
            product_db["productCode"],
            product_db["productName"],
            productline,
            product_db["productScale"],
            product_db["productVendor"],
            product_db["productDescription"],
            product_db["quantityInStock"],
            product_db["buyPrice"],
            product_db["MSRP"]
        ) 

        #JOIN with customer (for Location Object)

        customerNumber = order_db["customerNumber"]
        statement = "SELECT * FROM customers WHERE customerNumber=%s"
        r = getResult(statement,(customerNumber,))
        customer_db = r[0]

        #Create Location Object

        location = Location(
            customer_db["city"],
            customer_db["state"],
            customer_db["postalCode"],
            customer_db["country"]
        )

        #Create Order Object 

        sales = order_db["priceEach"]*order_db["quantityOrdered"]

        order = Order(
            order_db["orderDate"],
            product,
            location,
            order_db["quantityOrdered"],
            order_db["priceEach"],
            sales
        )

        orders.add(order)      

    #Write in Obect DB
    connection = newt.db.connection("postgresql://postgres:changeme@postgres:5432/newt")
    connection.root.orders = orders
    connection.commit()

createObjectsAndWriteInDB()