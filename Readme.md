# Server Requirements 
- docker
- docker-compose 

# Step 1: Starten der Container
```
docker-compose up -d
```
### Ports und Servies 
- MySQL-DB:
    - Port: 3306
- PHPMyAdmin: 
    - Port: 8000
- Postgres:  
    - Port: 5432
- PostgresAdmin: 
    - Port: 5050
- Theia:
    - Port: 3000

# Step 2: Builden des Theia Images 
```
docker build -t esb-theia -f  theia.Dockerfile .
```

# Step 3: Auf den Python Container per Bash zugreifen
```
docker exec -it python-comp bash
```
### Ausführen des ETL-Files 
```
python etl.py
```

# Step 4: OLAP-Operationen durchführen
### Connection zur DB und Aufrufen der gespeicherten Objekte
```python
connection = newt.db.connection("postgresql://postgres:changeme@postgres:5432/newt")
orders = connection.root.orders.orders
```
### Beispiele für Operationen
```python
#In welchem Land wurde im Jahr 2003 der meiste Umsatz erzielt? 
cube.addSliceDate("2003")
cube.aggregate("Location",["sum"])
cube.report()
cube.clear()

#Welches Produkt hat in Australien den meisten Umsatz in 2003 erzielt? 
cube.addSliceDate("2003")
cube.addSliceLocation("Australia")
cube.aggregate("Product",["sum"])
cube.report()
cube.clear()

#In welchem Jahr wurde der meiste Umsatz erzielt?
cube.aggregate("Date",["sum"])
cube.report()
cube.clear()
```
