FROM theiaide/theia-python:latest
COPY ./code /home/project
WORKDIR /home/project 
RUN pip3 install -r requirements.txt
RUN rm requirements.txt
RUN rm test.py
RUN rm etl.py
RUN rm main_sol.py
RUN rm -R __pycache__
WORKDIR /home/theia